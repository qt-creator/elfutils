/* Interface for libeu_compat.
   Copyright (C) 2018 The Qt Company Ltd.
   This file is part of elfutils.

   This file is free software; you can redistribute it and/or modify
   it under the terms of either

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at
       your option) any later version

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at
       your option) any later version

   or both in parallel, as here.

   elfutils is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see <http://www.gnu.org/licenses/>.  */

#ifndef _LIBEU_COMPAT_H
#define _LIBEU_COMPAT_H 1

#if (defined _WIN32 || defined __WIN32__)

#include <io.h>

#ifdef __cplusplus
extern "C" {
#endif

extern char *eu_compat_demangle(const char *mangled_name, char *output_buffer,
                                size_t *length, int *status);

extern int eu_compat_open(const char *, int);
extern int eu_compat_close(int);

extern void *eu_compat_malloc(size_t);
extern void *eu_compat_realloc(void *, size_t);
extern void *eu_compat_calloc(size_t);
extern void eu_compat_free(void *);

extern char* eu_compat_strdup(const char* string);

#ifdef __cplusplus
}
#endif

#else

#include <cxxabi.h>
#include <unistd.h>

#define eu_compat_demangle abi::__cxa_demangle

#define eu_compat_open open
#define eu_compat_close close
#define O_BINARY 0

#define eu_compat_malloc malloc
#define eu_compat_realloc realloc
#define eu_compat_calloc calloc
#define eu_compat_free free

#define eu_compat_strdup strdup

#endif

#endif // _LIBEU_COMPAT_H
